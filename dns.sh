#!/bin/bash
#=================================================================================
# AUTOR		: LUIZ PAULO PIMENTA
# DATA		: 29/12/2020
#=================================================================================
# RESOLVE PROBLEMA DE EXCLUSAO ARQUIVO RESOLV.CONF POR PARTE DO NETWORKMANAGER 
#=================================================================================

arquivo="/etc/NetworkManager/NetworkManager.conf"

#CRIA ARQUIVO DE CONFIGURACOES NETWORKMANAGER
echo "[main]" > $arquivo
echo "plugins=ifupdown,keyfile" >> $arquivo
echo "dns=none" >> $arquivo

echo "[ifupdown]" >> $arquivo
echo "managed=false" >> $arquivo

#REMOVE LINK SIMBOLICO RESOLV.CONF
rm /etc/resolv.conf

echo nameserver 8.8.8.8 > /etc/resolv.conf



