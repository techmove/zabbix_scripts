#!/bin/bash

#VERIFICA SERVICO
valida=$(service asterisk status | grep -i active | awk '{print $3}' | sed 's/(//g' | sed 's/)//g')


if [ $valida == "running" ] ; then

	echo 1;
else

	echo 0;

fi
