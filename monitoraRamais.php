#!/usr/bin/php -q
<?php
#=============================================================================
#	SCRIPT PARA MONITORAMENTO DO PERCENTUAL DE RAMAIS OFFLINE 
#=============================================================================

$curl = curl_init();

	curl_setopt_array($curl, array(
	CURLOPT_URL => "http://localhost/telmove/api/apiListadeRamais.php",
	CURLOPT_RETURNTRANSFER => true,
	CURLOPT_ENCODING => "",
	CURLOPT_MAXREDIRS => 10,
	CURLOPT_TIMEOUT => 0,
	CURLOPT_FOLLOWLOCATION => true,
	CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
	CURLOPT_CUSTOMREQUEST => "GET",
	));

$response = curl_exec($curl);

curl_close($curl);

$ramais=json_decode($response);


$online=0;
$offline=0;
$totalRamais=0;

foreach($ramais as $ramal ){
		
	$status=explode(">",$ramal->status);
	$status=str_replace("</span","",$status[1]);



	if($status == "ONLINE" ){
		$online++;
	}else{
		if( strtotime(date("Y-m-d H:i")) < strtotime(date("Y-m-d 18:00"))  && strtotime(date("Y-m-d H:i")) > strtotime(date("Y-m-d 08:00")) ){

			$offline++;

		}else{
			$online++;
		}	

	}

	$totalRamais++;
}	

$percentual=($offline * 100 ) / $totalRamais;

echo Round($percentual);

