#!/bin/bash
#+++++++++++++++++++++++++++++++++++
#	SCRIPT PARA CRIAR ACESSO VPN
#+++++++++++++++++++++++++++++++++++
#Autor : Luiz Paulo Pimenta
#Criado: 22/03/2021
#Atualizado : 22/03/2021
#+++++++++++++++++++++++++++++++++++
#Funcao : Apagar gravacoes antigas quando o disco estiver cheio
#+++++++++++++++++++++++++++++++++++


	#LIGACOES SAINTES
	ultimoMes=$(ls -lat /var/spool/asterisk/monitor/saintes | tail -n 2 | awk '{print $6}' | head -n 1)
	ultimoAno=$(ls -lat /var/spool/asterisk/monitor/saintes | tail -n 2 | awk '{print $8}' | head -n 1)
	ls -lat /var/spool/asterisk/monitor/saintes | grep -i $ultimoMes | grep -i $ultimoAno | grep -i $ultimoMes | awk '{print $9}' > /tmp/gravacoes_saintes.log

	#LIGACOES ENTRANTES 
	ultimoMes=$(ls -lat /var/spool/asterisk/monitor/entrantes | tail -n 2 | awk '{print $6}' | head -n 1)
	ultimoAno=$(ls -lat /var/spool/asterisk/monitor/entrantes | tail -n 2 | awk '{print $8}' | head -n 1)
	 ls -lat /var/spool/asterisk/monitor/entrantes| grep -i $ultimoMes | grep -i $ultimoAno | grep -i $ultimoMes | awk '{print $9}' > /tmp/gravacoes_entrantes.log


	#LIGACOES INTERNAS 
	ultimoMes=$(ls -lat /var/spool/asterisk/monitor/internas | tail -n 2 | awk '{print $6}' | head -n 1)
	ultimoAno=$(ls -lat /var/spool/asterisk/monitor/internas | tail -n 2 | awk '{print $8}' | head -n 1)
	 ls -lat /var/spool/asterisk/monitor/internas| grep -i $ultimoMes | grep -i $ultimoAno | grep -i $ultimoMes | awk '{print $9}' > /tmp/gravacoes_internas.log

	 while read gravacao
	 do

		   rm -rf /var/spool/asterisk/monitor/saintes/$gravacao

	  done < /tmp/gravacoes_saintes.log

	  while read gravacao
	  do

		   rm -rf /var/spool/asterisk/monitor/entrantes/$gravacao

	  done < /tmp/gravacoes_entrantes.log

	  while read gravacao
	  do

		   rm -rf /var/spool/asterisk/monitor/internas/$gravacao

	  done < /tmp/gravacoes_internas.log


