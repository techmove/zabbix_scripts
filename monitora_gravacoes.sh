#!/bin/bash
#+++++++++++++++++++++++++++++++++++
#	SCRIPT PARA CRIAR ACESSO VPN
#+++++++++++++++++++++++++++++++++++
#Autor : Luiz Paulo Pimenta
#Criado: 22/03/2021
#Atualizado : 22/03/2021
#+++++++++++++++++++++++++++++++++++
#Funcao : Apagar gravacoes antigas quando o disco estiver cheio
#+++++++++++++++++++++++++++++++++++


#VALIDA LOCAL DAS GRAVACOES
gravacoes=$( df -h /var/spool/asterisk/monitor | awk '{print $5}' | tail -n 1 |sed 's/%//g')

	if [ -n $gravacoes ] ; then


		uso=$( df -h | grep -i var | awk '{print $5}' | sed 's/%//g')

		echo  $uso

	else

		echo 0 


	fi



